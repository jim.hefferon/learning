# Learning summary sheet


## Summary

One page sheet summarizing what is known about how excel at learning.  Intended as an introduction for college students.


## License

This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.


## Install

This is written in LaTeX and 
compiled with xelatex.


## Version

2017-Dec


## Author

Jim Hefferon

### Sources
Based on information widely available, notably from *Peak* by Ericsson and Pool and *Make it Stick* by Brown, Roediger, and McDaniel.

